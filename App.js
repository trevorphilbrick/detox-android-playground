/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {fetchUserData} from './api/randomUserFetch';

const App = () => {
  const [section, setSection] = useState('section1');
  const [userData, setUserData] = useState(null);

  const setData = async () => {
    const data = await fetchUserData();
    setUserData(data);
  };

  useEffect(() => {
    setTimeout(() => {
      setData();
    }, 4000);
  }, []);

  const renderSection = () => {
    if (section === 'section1') {
      return (
        <View testID="homeSection">
          <Text style={{textAlign: 'center'}}>Home</Text>
        </View>
      );
    } else if (section === 'section2') {
      return (
        <View testID="friendsSection">
          {userData ? <FriendFeed userData={userData}  /> : <Text>Loading</Text>}
        </View>
      );
    }
  };

  return (
    <View style={styles.container}>
      <Text style={{textAlign: 'center', fontSize: 18, fontWeight: 'bold'}}>
        Detox Demo
      </Text>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
          justifyContent: 'space-around',
          marginVertical: 20,
        }}>
        <TouchableOpacity
          onPress={() => setSection('section1')}
          testID="homeButton">
          <Text>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setSection('section2')}
          testID="friendsButton">
          <Text>Friends</Text>
        </TouchableOpacity>
      </View>
      {renderSection()}
    </View>
  );
};

const FriendFeed = ({userData}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [active, setActive] = useState(null);

  const renderUserCards = ({item}) => {
    const displayModal = () => {
      setActive(item);
      setModalVisible(!modalVisible);
    };

    return (
      <TouchableOpacity onPress={() => displayModal()}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            margin: 5,
            padding: 5,
          }}>
          <Image source={{uri: item.avatar}} style={{width: 60, height: 60}} />
          <View>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
              }}>{`${item.first_name} ${item.last_name}`}</Text>
            <Text>{item.username}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View testID="friendFeed">
      <FlatList
        data={userData}
        renderItem={renderUserCards}
        keyExtractor={item => item.id}
      />
      <Modal visible={modalVisible}>
        <TouchableOpacity onPress={() => setModalVisible(false)}>
          <Text style={{textAlign: 'right', margin: 10}}>close</Text>
        </TouchableOpacity>
        {active ? <UserInfo active={active} /> : <Text>Loading...</Text>}
      </Modal>
    </View>
  );
};

const UserInfo = ({active}) => (
  <View>
  <View
    style={{
      alignItems: 'center',
      backgroundColor: '#707070',
      paddingVertical: 10,
    }}>
    <Image
      source={{uri: active.avatar}}
      style={{
        width: 130,
        height: 130,
        borderRadius: 100,
        backgroundColor: '#0cc',
      }}
    />
    <Text style={{color:"#fff", fontSize: 22, fontWeight: 'bold'}}>{`${active.first_name} ${active.last_name}`}</Text>
  </View>
  
  </View>
);

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
