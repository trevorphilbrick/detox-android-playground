Setting up and running detox environment- Android 

BUILD TESTING APP

    To build a test app run the following command:

        detox build  -c <configuration> 

        ex.: detox build -c android

RUNNING TESTS

    To run a test, run the following command:

        detox test <test.defined.in.e2e> -c <configuration> 

        ex.: detox test e2e/anotherTest.e2e.js -c android

WRITING TESTS 

    Detox has a wide variety of selectors and utilities that can be used to run different tests. 

    We can create tests inside of our "e2e" folder. To create a new test, we create a file with the extension ".e2e.js". 
    In this project you can see two tests created. 

    Below are some additional resources to understand more what can be done with testing:

        Start here: Writing our first test (https://wix.github.io/Detox/docs/introduction/writing-first-test)

        For more details on the Detox Object go here (https://wix.github.io/Detox/docs/api/detox-object-api)
        For more details on the Device Object go here (https://wix.github.io/Detox/docs/api/device-object-api)
ENVIRONMENT SETUP 

    Pre-install:

        Ensure there is an android emulator installed.

            run "adb devices" in BASH 

        Ensure Java is version  >=1.8

            run "java -version" in BASH

        Ensure ANDROID_HOME is set up as an environment variable

    Install Detox to your project:

        "npm install detox --save-dev"
    
    Install a test-runner

        Detox supports both Jest and Mocha, but for this environment we are going to set up Jest, as it is the recommended testing framework.

        To install Jest r the following:

            "yarn add --dev jest"

    Apply Detox configuration(for both IOS and Android)

        Now, we want to initialize detox by running:

            "detox init -r jest"
        
        Which will create a new directory called "e2e". Inside this directory we will find a "config.json", "environment.js", and "firstTest.e2e.js" file.

        You can find a list here (https://wix.github.io/Detox/docs/guide/jest#3-fix--verify) to ensure that these files contain what they need to for now. 

    Initializing Detox for Android

        Detox gives us the option of setting up our testing configurations in the detoxrc.json installed by detox OR inside of our package.json. For this set up we will do it inside of the detoxrc.json file. 

        By default the config object inside of deoxrc.json looks something like this:

            <CODEBLOCK>
    {
        "testRunner": "jest",
        "runnerConfig": "e2e/config.json",
        "skipLegacyWorkersInjection": true,
        "apps": {
            "ios": {
            "type": "ios.app",
            "binaryPath": "SPECIFY_PATH_TO_YOUR_APP_BINARY"
            },
            "android": {
            "type": "android.apk",
            "binaryPath": "SPECIFY_PATH_TO_YOUR_APP_BINARY"
            }
        },
        "devices": {
            "simulator": {
            "type": "ios.simulator",
            "device": {
                "type": "iPhone 11"
            }
            },
            "emulator": {
            "type": "android.emulator",
            "device": {
                "avdName": "Pixel_3a_API_30_x86"
            }
            }
        },
        "configurations": {
            "ios": {
            "device": "simulator",
            "app": "ios"
            },
            "android": {
            "device": "emulator",
            "app": "android"
            }
        }
    }
            </CODEBLOCK>
        
        We need to make a few changes inside of here. First, we want to set the avdName under devices.emulator.device to the name of our emulator. To find that name, first, make sure your emulator device is running. Second, enter into BASH:

            "adb -s emulator-5554 emu avd name"
        
        and this should return the name of your device. In my case it is "Pixel_5_API_30".

        ANDROID/BUILD.GRADLE

            inside of the android/build.gradle we need to add under allprojects.repositories: 

                'google()
                maven {
                // All of Detox' artifacts are provided via the npm module
                url "$rootDir/../node_modules/detox/Detox-android"'

            We also have to add Kotlin to the project under buildscripts:

                "kotlinVersion = '1.3.0' // (check what the latest version is! as of now it is 1.5.20. this can be checked in android studio)"

            And add under buildscripts.dependencies:

                'classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"'

                

            Note: if allprojects doesn't exist it needs to be created.

        ANDROID/APP/BUILD.GRADLE

            inside of the android/app/build.gradle we need to add under dependencies:

                "androidTestImplementation('com.wix:detox:+')"
            
            and inside the defaultconfig add: 

                "testBuildType System.getProperty('testBuildType', 'debug') 
                testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'"
            
            As well as:
                
                "testBuildType System.getProperty('testBuildType', 'debug')  // This will later be used to control the test apk build type
                testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'
                missingDimensionStrategy 'detox', 'full'"

            We need to add a ruleset to Proguard. To do so, under buildTypes.release add:

                'proguardFile "${rootProject.projectDir}/../node_modules/detox/android/detox/proguard-rules-app.pro"'

            As well as add the following under dependencies:

                "androidTestImplementation(project(path: ":detox"))"

        We now need to create a detox test class.

            First, we need to add this file and file-path to our project:

                "android/app/src/androidTest/java/com/[your.package]/DetoxTest.java"
            
            And then paste the content from here (https://github.com/wix/Detox/blob/master/examples/demo-react-native/android/app/src/androidTest/java/com/example/DetoxTest.java) inside. 

            Next, replace all instances of "example" with our project's name. 
        
            According to the Detox documentation Test Butler is optional, but comes partially activated right out of the box. To keep this from interfering, We comment out the following in DetoxTest.java:

                "//TestButlerProbe.assertReadyIfInstalled();"

        We need to enable clear-text (unencrypted) traffic for Detox

            If the following file-path does not exist, we need to create it:

                "android/app/src/main/res/xml/network_security_config.xml"
            
            and inside of it paste the following:

"<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">10.0.2.2</domain>
        <domain includeSubdomains="true">localhost</domain>
    </domain-config>
</network-security-config>"

        Then, add the following to the AndroidManifest.xml opening application tag:

            'android:networkSecurityConfig="@xml/network_security_config"'

        ANDROID/SETTINGS.GRADLE

            We need to set up Detox as compiling depency. To do so we add to our settings.gradle file:

            "include ':detox'
            project(':detox').projectDir = new File(rootProject.projectDir, '../node_modules/detox/android/detox')"

        
        
        



            