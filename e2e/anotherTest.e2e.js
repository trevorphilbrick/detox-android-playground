describe('Test Sections', () => {
    beforeAll(async () => {
      await device.launchApp();
    });
  
    beforeEach(async () => {
      await device.reloadReactNative();
    });
  
    it('should trigger section 1', async () => {
      await element(by.id('buttonOne')).tap();
      await expect(element(by.id('sectionOne'))).toBeVisible()
    })

    it('should trigger section 2', async () => {
      await element(by.id('buttonTwo')).tap();
      await expect(element(by.id('sectionTwo'))).toBeVisible()
    })
    
    it('should trigger section 1', async () => {
        await element(by.id('buttonOne')).tap();
        await expect(element(by.id('sectionOne'))).toBeVisible()
      })
  
  });