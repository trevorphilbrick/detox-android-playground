export const fetchUserData = async () => {
  const userData = await fetch('https://random-data-api.com/api/users/random_user?size=50')
    .then(response => response.json())

  return userData;
};
